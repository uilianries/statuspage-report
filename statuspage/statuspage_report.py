import requests
import platform
from argparse import ArgumentParser
import subprocess
import getpass
from datetime import datetime, timezone, timedelta


def get_arguments(argv):
    parser = ArgumentParser("Manage status page incidents")


    subparser = parser.add_subparsers(dest='command')
    subparser.add_parser("create", help="Create a new incident")
    subparser.add_parser("update", help="Update an existing incident")
    subparser.add_parser("maintenance", help="Start a maintenance window")
    subparser.add_parser("resolve", help="Ends an incident")

    parser.add_argument("-ic", "--incident", type=str, help="Incident ID to be updated")
    parser.add_argument("-t", "--title", type=str, help="Incident title")
    parser.add_argument("-m", "--message", type=str, help="Incident body description")
    parser.add_argument("-s", "--status", help="Incident status",
                        choices=["investigating", "identified", "monitoring", "resolved"])
    parser.add_argument("-i", "--impact", help="Incident impact",
                        choices=["none", "maintenance", "minor", "major", "critical"])
    parser.add_argument("-cs", "--component-status", help="Component status",
                        choices=["operational", "degraded_performance", "partial_outage", "major_outage"])
    parser.add_argument("-ks", "--keychain-service", type=str, help="Keychain service", default="statuspage-token")
    parser.add_argument("-tk", "--token", type=str, help="Status Page API token")
    parser.add_argument("-p", "--page", type=str, help="Status Page ID", default="wlv9pfg6x1nh")
    parser.add_argument('-c', "--components", help="Incident components", nargs="+", default=['f0knlpl991g7'])
    parser.add_argument('-g', '--ignore-ssl', action='store_true', help="Ignore SSL verification")
    parser.add_argument('-u', '--user', type=str, help="Status Page username")
    parser.add_argument('-sh', "--scheduled", type=str, help="Scheduled maintenance", default="now")
    parser.add_argument('-e', '--event', help="Event to be finished", choices=["maintenance", "incident"])
    args, _ = parser.parse_known_args(argv)
    return parser, args


def show_help(parser):
    parser.print_help()
    exit(1)


def get_token(args):
    token = args.token
    if not args.token and platform.system() == "Darwin":
        # INFO: https://scriptingosx.com/2021/04/get-password-from-keychain-in-shell-scripts/
        # security add-generic-password -s 'CLI Test'  -a 'armin' -w 'password123'
        # security find-generic-password -w -s 'CLI Test' -a 'armin'
        user = args.user or getpass.getuser()
        print(f"Getting Status Page token from keychain for user '{user}'")
        output = subprocess.run(["security", "find-generic-password", "-a", user, "-s", args.service, "-w"], capture_output=True, text=True)
        token = output.stdout.strip()
    return token


def create_incident(token, args):
    # https://developer.statuspage.io/#operation/postPagesPageIdIncidents
    url = f"https://api.statuspage.io/v1/pages/{args.page}/incidents"
    comp_status = args.component_status or 'major_outage'
    payload = {
        "incident": {
            "name": args.title,
            "status": args.status,
            "body": args.message,
            "components": {component: comp_status for component in args.components},
            "component_ids": args.components,
            "impact_override": args.impact,
        }
    }
    print(f"Creating a new incident: {args.title}")
    headers = {"Authorization": f"OAuth {token}", "Content-Type": "application/json"}
    response = requests.post(url, json=payload, headers=headers, verify=not args.ignore_ssl)
    response.raise_for_status()
    print(f"Response ({response.status_code}):\n{response.json()}")


def update_incident(token, args):
    # https://developer.statuspage.io/#operation/patchPagesPageIdIncidentsIncidentId
    assert args.incident, "Incident ID is required."
    url = f"https://api.statuspage.io/v1/pages/{args.page}/incidents/{args.incident}"
    payload = {"incident": {}}
    if args.title:
        payload["incident"]["name"] = args.title
    if args.status:
        payload["incident"]["status"] = args.status
    if args.message:
        payload["incident"]["body"] = args.message
    if args.component_status:
        payload["incident"]["components"] = {component: args.component_status for component in args.components}
        payload["incident"]["component_ids"] = args.components
    if args.impact:
        payload["incident"]["impact_override"] = args.impact
    headers = {"Authorization": f"OAuth {token}", "Content-Type": "application/json"}
    print(f"Updating the existing incident {args.incident}")
    response = requests.patch(url, json=payload, headers=headers, verify=not args.ignore_ssl)
    response.raise_for_status()
    print(f"Response ({response.status_code}):\n{response.json()}")


def create_maintenance(token, args):
    url = f"https://api.statuspage.io/v1/pages/{args.page}/incidents"
    # scheduled_for
    sched = args.scheduled or 'now'
    if sched == 'now':
        current_time_utc = datetime.now(timezone.utc)
        new_time_utc = current_time_utc + timedelta(minutes=1)
        sched = new_time_utc.strftime("%Y-%m-%dT%H:%M:%S.%fZ")
    # scheduled_until
    sched_until = datetime.strptime(sched, "%Y-%m-%dT%H:%M:%S.%fZ") + timedelta(hours=8)
    sched_until = sched_until.strftime("%Y-%m-%dT%H:%M:%S.%fZ")
    payload = {
        "incident": {
            "name": args.title,
            "impact_override": 'maintenance',
            "status": 'scheduled',
            "scheduled_for": sched,
            "scheduled_until": sched_until,
            "scheduled_remind_prior": True,
            "auto_transition_to_maintenance_state": True,
            "auto_transition_to_operational_state": True,
            "scheduled_auto_in_progress": True,
            "scheduled_auto_completed": False,
            "auto_transition_deliver_notifications_at_start": False,
            "auto_transition_deliver_notifications_at_end": True,
            "deliver_notifications": True,
            "body": args.message,
            "components": {component: 'under_maintenance' for component in args.components},
            "component_ids": args.components,
        }
    }
    print(f"Creating a new scheduled maintenance: {args.title}")
    headers = {"Authorization": f"OAuth {token}", "Content-Type": "application/json"}
    response = requests.post(url, json=payload, headers=headers, verify=not args.ignore_ssl)
    response.raise_for_status()
    print(f"Response ({response.status_code}):\n{response.json()}")


def resolve_incident(token, args):
    assert args.incident, "Incident ID is required."
    assert args.event, "Event type is required."
    status = 'resolved' if args.event == 'incident' else 'completed'
    url = f"https://api.statuspage.io/v1/pages/{args.page}/incidents/{args.incident}"
    payload = {
        "incident": {
            "status": status,
            "impact_override": 'none',
            "components": {component: 'operational' for component in args.components},
            "component_ids": args.components,
        }
    }
    headers = {"Authorization": f"OAuth {token}", "Content-Type": "application/json"}
    print(f"Ending the incident {args.incident}")
    response = requests.patch(url, json=payload, headers=headers, verify=not args.ignore_ssl)
    response.raise_for_status()
    print(f"Response ({response.status_code}):\n{response.json()}")


def main(argv=None):
    parser, args = get_arguments(argv)
    token = get_token(args)
    assert token, "Status Page API token is required."

    if args.command == "create":
        create_incident(token, args)
    elif args.command == "update":
        update_incident(token, args)
    elif args.command == "maintenance":
        create_maintenance(token, args)
    elif args.command == "resolve":
        resolve_incident(token, args)
    else:
        show_help(parser)


if __name__ == "__main__":
    main()
