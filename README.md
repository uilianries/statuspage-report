# Status Page Reporter

Create and update incidents and maintenances on statuspage.io

## INSTALL

python setup.py install

## USAGE

#### Token API (Authentication)

To obtain a Token API, you need to go to your statuspage.io account and generate a new token.

It should be available at the following URL: https://manage.statuspage.io/organizations/<your_organization>/api-info

#### Create a new incident 

To create a new incident you need to provide some parameters:

* --token: Your API token
* --page: Your page ID, it can be found in the URL of your statuspage.io
* -s: The status of the incident (investigating, identified, monitoring, resolved)
* -i: The impact of the incident (none, minor, major, critical)
* -cs: The component status (operational, degraded_performance, partial_outage, major_outage)
* -c: The component ID, it can be found in the URL of your statuspage.io (e.g API, Web, etc)
* -t: The title of the incident
* -m: The message of the incident (It does not accept new lines)

Once the command is executed, a JSON will be returned as answer. The incident ID will be available in the JSON as 'id'.

```
statuspage-report --token=<yourapitoken> --page=zbr44r824wrq -s investigating -i major -cs major_outage -c htr39snqsm2p -t "Jenkins is overloaded" -m "Our jenkins is overloaded, but we are working on it. Sorry!" create
```

#### Update the current incident

It's useful to update the current incident with new information.
It works in the same way as the create command, but you need to provide the incident ID.

```
statuspage-report --token=<yourapitoken> --page=zbr44r824wrq -s identified -m "We found the root problem and we are working on it." -ic zw7gzcflqp9b update
```

#### Resolve the current incident

When an incident is resolved, you need to provide the incident ID and the component ID (if it's not provided, it will not resolve any component).

```
statuspage-report --token=<yourapitoken> --page=zbr44r824wrq -ic=rxjkk6zvm0nl -e incident —components=htr39snqsm2p resolve
```

#### Schedule a new maintenance for now

Maintenance is a little bit different than incident, because you need to provide a start date and an end date.
The start date and end date are in ISO 8601 format (YYYY-MM-DDTHH:MM:SSZ). In case you dot not provide a start date, it will use the current date and time.

```
statuspage-report --token=<yourapitoken> --page=zbr44r824wrq -c htr39snqsm2p -t "Weekly maintenance" -m "Weekly internal maintenance" maintenance 
```

#### Complete the current maintenance 

To finish the current maintenance, you need to provide the maintenance ID and the component ID (if it's not provided, it will not resolve any component).
The parameter -e (event) should be maintenance, the statuspage process maintenance and incident in different way.

```
statuspage-report --token=<yourapitoken> --page=zbr44r824wrq -ic=f1htlbnl9w0k -e maintenance —components=htr39snqsm2p resolve
```

## LICENSE
![MIT](LICENSE)
