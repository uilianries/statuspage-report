import responses
from responses import matchers

from statuspage.statuspage_report import main


@responses.activate
def test_create_incident():
    """Test if capable to create a new incident in Status Page."""
    argv = [
            "--token=0xdeadbeef",
            "--page=page0xc0ffee",
            "--status=identified",
            "--impact=minor",
            "--component-status=major_outage",
            "--components=component0xs0da",
            "--title=Cogito, ergo sum",
            "--message=I think, therefore I am",
            "create",
        ]
    json_response = [
        {
            "id": "p31zjtct2jer",
            "components": [
                {
                    "id": "component0xs0da",
                    "page_id": "page0xc0ffee",
                    "group_id": "group0x5ca1ab1e",
                    "created_at": "2023-12-19T08:44:40Z",
                    "updated_at": "2023-12-19T08:44:40Z",
                    "group": True,
                    "name": "API Service",
                    "description": "API Service",
                    "position": 0,
                    "status": "minor_outage",
                    "showcase": True,
                    "only_show_if_degraded": False,
                    "automation_email": "email@acme.com",
                    "start_date": "2023-12-19"
                }
            ],
            "created_at": "2023-12-19T08:44:40Z",
            "impact": "minor",
            "impact_override": "minor",
            "monitoring_at": "2023-12-19T08:44:40Z",
            "name": "Cogito, ergo sum",
            "page_id": "page0xc0ffee",
            "scheduled_auto_completed": False,
            "scheduled_auto_in_progress": False,
            "scheduled_for": "2013-05-07T03:00:00.007Z",
            "auto_transition_deliver_notifications_at_end": False,
            "auto_transition_deliver_notifications_at_start": True,
            "auto_transition_to_maintenance_state": True,
            "auto_transition_to_operational_state": True,
            "scheduled_remind_prior": True,
            "scheduled_reminded_at": "2023-12-19T08:44:40Z",
            "status": "identified",
            "updated_at": "2023-12-19T08:44:40Z",
            "reminder_intervals": "[3, 6, 12, 24]"
        }
    ]
    responses.add(responses.POST, 'https://api.statuspage.io/v1/pages/page0xc0ffee/incidents', json=json_response,
                  match=[matchers.json_params_matcher({
                      "incident": {
                          "body": "I think, therefore I am",
                          "component_ids": ["component0xs0da"],
                          "components": {"component0xs0da": "major_outage"},
                          "impact_override": "minor",
                          "name": "Cogito, ergo sum",
                          "status": "identified"
                      }
                  })])
    main(argv)


@responses.activate
def test_update_incident():
    """Test if capable to update an existing incident in Status Page."""
    argv = [
        "--token=0xdeadbeef",
        "--page=page0xc0ffee",
        "--status=identified",
        "--impact=minor",
        "--component-status=major_outage",
        "--components=component0xs0da",
        "--title=Cogito, ergo sum",
        "--message=I think, therefore I am",
        "--incident=incident0xf00bar",
        "update",
    ]
    json_response = [
        {
            "id": "incident0xf00bar",
            "components": [
                {
                    "id": "component0xs0da",
                    "page_id": "page0xc0ffee",
                    "group_id": "group0x5ca1ab1e",
                    "created_at": "2023-12-19T08:44:40Z",
                    "updated_at": "2023-12-19T08:44:40Z",
                    "group": True,
                    "name": "API Service",
                    "description": "API Service",
                    "position": 0,
                    "status": "minor_outage",
                    "showcase": True,
                    "only_show_if_degraded": False,
                    "automation_email": "email@acme.com",
                    "start_date": "2023-12-19"
                }
            ],
            "created_at": "2023-12-19T08:44:40Z",
            "impact": "minor",
            "impact_override": "minor",
            "monitoring_at": "2023-12-19T08:44:40Z",
            "name": "Cogito, ergo sum",
            "page_id": "page0xc0ffee",
            "scheduled_auto_completed": False,
            "scheduled_auto_in_progress": False,
            "scheduled_for": "2013-05-07T03:00:00.007Z",
            "auto_transition_deliver_notifications_at_end": False,
            "auto_transition_deliver_notifications_at_start": True,
            "auto_transition_to_maintenance_state": True,
            "auto_transition_to_operational_state": True,
            "scheduled_remind_prior": True,
            "scheduled_reminded_at": "2023-12-19T08:44:40Z",
            "status": "identified",
            "updated_at": "2023-12-19T08:44:40Z",
            "reminder_intervals": "[3, 6, 12, 24]"
        }
    ]
    responses.add(responses.PATCH, 'https://api.statuspage.io/v1/pages/page0xc0ffee/incidents/incident0xf00bar', json=json_response,
                  match=[matchers.json_params_matcher({
                      "incident": {
                          "body": "I think, therefore I am",
                          "component_ids": ["component0xs0da"],
                          "components": {"component0xs0da": "major_outage"},
                          "impact_override": "minor",
                          "name": "Cogito, ergo sum",
                          "status": "identified"
                      }
                  })])
    main(argv)


@responses.activate
def test_create_maintenance():
    """Test if capable to create a maintenance window in Status Page."""
    argv = [
        "--token=0xdeadbeef",
        "--page=page0xc0ffee",
        "--components=component0xs0da",
        "--title=Cogito, ergo sum",
        "--message=I think, therefore I am",
        "maintenance",
    ]
    json_response = [
        {
            "id": "p31zjtct2jer",
            "components": [
                {
                    "id": "component0xs0da",
                    "page_id": "page0xc0ffee",
                    "group_id": "group0x5ca1ab1e",
                    "created_at": "2023-12-19T08:44:40Z",
                    "updated_at": "2023-12-19T08:44:40Z",
                    "group": True,
                    "name": "API Service",
                    "description": "API Service",
                    "position": 0,
                    "status": "minor_outage",
                    "showcase": True,
                    "only_show_if_degraded": False,
                    "automation_email": "email@acme.com",
                    "start_date": "2023-12-19"
                }
            ],
            "created_at": "2023-12-19T08:44:40Z",
            "impact": "minor",
            "impact_override": "minor",
            "monitoring_at": "2023-12-19T08:44:40Z",
            "name": "Cogito, ergo sum",
            "page_id": "page0xc0ffee",
            "scheduled_auto_completed": False,
            "scheduled_auto_in_progress": False,
            "scheduled_for": "2013-05-07T03:00:00.007Z",
            "auto_transition_deliver_notifications_at_end": False,
            "auto_transition_deliver_notifications_at_start": True,
            "auto_transition_to_maintenance_state": True,
            "auto_transition_to_operational_state": True,
            "scheduled_remind_prior": True,
            "scheduled_reminded_at": "2023-12-19T08:44:40Z",
            "status": "identified",
            "updated_at": "2023-12-19T08:44:40Z",
            "reminder_intervals": "[3, 6, 12, 24]"
        }
    ]
    responses.add(responses.POST, 'https://api.statuspage.io/v1/pages/page0xc0ffee/incidents', json=json_response,
                  match=[matchers.json_params_matcher({
                      "incident": {
                          "auto_transition_deliver_notifications_at_end": True,
                          "auto_transition_deliver_notifications_at_start": False,
                          "auto_transition_to_maintenance_state": True,
                          "auto_transition_to_operational_state": True,
                          "body": "I think, therefore I am",
                          "component_ids": ["component0xs0da"],
                          "components": {"component0xs0da": "under_maintenance"},
                          "deliver_notifications": True,
                          "impact_override": "maintenance",
                          "name": "Cogito, ergo sum",
                          "scheduled_auto_completed": False,
                          "scheduled_auto_in_progress": True,
                          "scheduled_remind_prior": True,
                          "status": "scheduled"
                      }
                  }, strict_match=False)])
    main(argv)
    assert len(responses.calls) == 1
    assert "scheduled_for" in responses.calls[0].request.body.decode('utf-8')
    assert "scheduled_until" in responses.calls[0].request.body.decode('utf-8')


@responses.activate
def test_resolve_incident():
    """Test if capable to resolve an existing incident in Status Page."""
    argv = [
        "--token=0xdeadbeef",
        "--page=page0xc0ffee",
        "--components=component0xs0da",
        "--event=incident",
        "--incident=incident0xf00bar",
        "resolve",
    ]
    json_response = [
        {
            "id": "incident0xf00bar",
            "components": [
                {
                    "id": "component0xs0da",
                    "page_id": "page0xc0ffee",
                    "group_id": "group0x5ca1ab1e",
                    "created_at": "2023-12-19T08:44:40Z",
                    "updated_at": "2023-12-19T08:44:40Z",
                    "group": True,
                    "name": "API Service",
                    "description": "API Service",
                    "position": 0,
                    "status": "operational",
                    "showcase": True,
                    "only_show_if_degraded": False,
                    "automation_email": "email@acme.com",
                    "start_date": "2023-12-19"
                }
            ],
            "created_at": "2023-12-19T08:44:40Z",
            "impact": "none",
            "impact_override": "none",
            "monitoring_at": "2023-12-19T08:44:40Z",
            "name": "Cogito, ergo sum",
            "page_id": "page0xc0ffee",
            "scheduled_auto_completed": False,
            "scheduled_auto_in_progress": False,
            "scheduled_for": "2013-05-07T03:00:00.007Z",
            "auto_transition_deliver_notifications_at_end": False,
            "auto_transition_deliver_notifications_at_start": True,
            "auto_transition_to_maintenance_state": True,
            "auto_transition_to_operational_state": True,
            "scheduled_remind_prior": True,
            "scheduled_reminded_at": "2023-12-19T08:44:40Z",
            "status": "resolved",
            "updated_at": "2023-12-19T08:44:40Z",
            "reminder_intervals": "[3, 6, 12, 24]"
        }
    ]
    responses.add(responses.PATCH, 'https://api.statuspage.io/v1/pages/page0xc0ffee/incidents/incident0xf00bar',
                  json=json_response,
                  match=[matchers.json_params_matcher({
                      "incident": {
                          "component_ids": ["component0xs0da"],
                          "components": {"component0xs0da": "operational"},
                          "impact_override": "none",
                          "status": "resolved"
                      }
                  })])
    main(argv)
